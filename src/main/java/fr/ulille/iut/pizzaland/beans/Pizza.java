package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	private long id;
	private String name;
	private List<Ingredient> ingredients;

	public Pizza() {
	}

	public Pizza(long id, String name, List<Ingredient> list) {
		this.id = id;
		this.name = name;
		ingredients = list;
	}

	public Pizza(long id, String name) {
		this.id = id;
		this.name = name;
		ingredients = new ArrayList<Ingredient>();
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> list) {
		this.ingredients = list;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static PizzaDto toDto(Pizza i) {
		PizzaDto dto = new PizzaDto();
		dto.setId(i.getId());
		dto.setName(i.getName());
		List<Long> l = new ArrayList<Long>();
		if (i.ingredients != null)
			for (Ingredient ing : i.getIngredients()) {
				l.add(ing.getId());
			}
		dto.setIntegers(l);
		return dto;
	}

	public static Pizza fromDto(PizzaDto dto) {
		Pizza Pizza = new Pizza();
		Pizza.setId(dto.getId());
		Pizza.setName(dto.getName());

		return Pizza;
	}

	public static PizzaCreateDto toCreateDto(Pizza Pizza) {
		PizzaCreateDto dto = new PizzaCreateDto();
		dto.setName(Pizza.getName());
		dto.setIngredients(Pizza.getIngredients());
		return dto;
	}

	public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
		Pizza pizza = new Pizza();
		pizza.setName(dto.getName());
		pizza.setIngredients(dto.getIngredients());
		return pizza;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		String tmp = "";
		for (Ingredient ingredient : ingredients) {
			tmp += ingredient.toString() + " , ";
		}
		return "Pizza [id=" + id + ", name=" + name + ",ingredients=" + ingredients.toString() + "]";
	}
}
