package fr.ulille.iut.pizzaland.resources;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("/pizzas")
public class PizzaResource {
	private static final Logger LOGGER = Logger.getLogger(PizzaResource.class.getName());
	@Context
	public UriInfo uriInfo;

	private PizzaDao pizzas;
	private IngredientDao ingredients;

	public PizzaResource() {
		ingredients = BDDFactory.buildDao(IngredientDao.class);
		pizzas = BDDFactory.buildDao(PizzaDao.class);
		pizzas.createTableAndIngredientAssociation();
	}

	@POST
	public Response createIngredient(PizzaCreateDto pizzaCreateDto) {
		Pizza existing = pizzas.findByName(pizzaCreateDto.getName());
		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}

		try {
			Pizza pizza = Pizza.fromPizzaCreateDto(pizzaCreateDto);
			long id = pizzas.insert(pizza.getName());
			pizza.setId(id);
			PizzaDto pizzaDto = Pizza.toDto(pizza);

			URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();

			return Response.created(uri).entity(pizzaDto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}

	@GET
	public List<PizzaDto> getAll() {
		LOGGER.info("PizzasResource:getAll");

		List<PizzaDto> l = pizzas.getAll().stream().map(Pizza::toDto).collect(Collectors.toList());
		for (PizzaDto pizzaDto : l) {
			pizzaDto.setIntegers(	pizzas.getAllIngredientsbyIdPizza(pizzaDto.getId())
			);
		}
		return l;
	}

	@GET
	@Path("{id}")
	public PizzaDto getOnePizza(@PathParam("id") long id) {
		LOGGER.info("getOnePizza(" + id + ")");

		try {
			Pizza pizza = pizzas.findById(id);
			return Pizza.toDto(pizza);
		} catch (Exception e) {
			// Cette exception générera une réponse avec une erreur 404
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}

	@GET
	@Path("{id}/name")
	public String getPizzaName(@PathParam("id") long id) {
		Pizza pizza = pizzas.findById(id);
		if (pizza == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return pizza.getName();
	}

	@POST
	@Consumes("application/x-www-form-urlencoded")
	public Response createPizza(@FormParam("name") String name) {
		Pizza existing = pizzas.findByName(name);
		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}

		try {
			Pizza pizza = new Pizza();
			pizza.setName(name);

			long id = pizzas.insert(pizza.getName());
			pizza.setId(id);
			PizzaDto pizzaDto = Pizza.toDto(pizza);

			URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();

			return Response.created(uri).entity(pizzaDto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}

	@DELETE
	@Path("{id}")
	public Response deletePizza(@PathParam("id") long id) {
		if (pizzas.findById(id) == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		pizzas.remove(id);

		return Response.status(Response.Status.ACCEPTED).build();
	}

	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Path("{id}/ajout")
	public Response ajoutIngredient(@PathParam("id") long idp, @FormParam("idi") long idi) {
		Pizza existing = pizzas.findById(idp);
		if (existing == null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}
		Ingredient existingi = ingredients.findById(idi);
		if (existingi == null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}

		try {

			pizzas.ajoutIngredient(idi, idp);
			Pizza pizza = new Pizza();
			pizza.setId(idp);
			List<Ingredient> l = new ArrayList<Ingredient>();
			List<Long> i = pizzas.getAllIngredientsbyIdPizza(idp);
			for (Long integer : i)
				l.add(ingredients.findById(integer));
			pizza.setName(existing.getName());
			pizza.setIngredients(l);
			System.out.println(pizza);
			PizzaDto pizzaDto = Pizza.toDto(pizza);

			URI uri = uriInfo.getAbsolutePathBuilder().path("" + idp).build();

			return Response.created(uri).entity(pizzaDto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}

	}

}
