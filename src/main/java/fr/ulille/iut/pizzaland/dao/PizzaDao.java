package fr.ulille.iut.pizzaland.dao;

import java.util.HashMap;
import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {

	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizza (id INTEGER PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
	void createTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (idp integer , idi integer , PRIMARY KEY (idp,idi),     FOREIGN KEY (idp) REFERENCES pizza(id),      FOREIGN KEY (idi) REFERENCES ingredients(id))")
	void createAssociationTable();

	@Transaction
	default void createTableAndIngredientAssociation() {
		createAssociationTable();
		createTable();
	}

	@SqlUpdate("DROP TABLE IF EXISTS pizza")
	void dropTable();

	@SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation")
	void dropTableAsso();

	@SqlUpdate("INSERT INTO pizza (name) VALUES (:name)")
	@GetGeneratedKeys
	long insert(String name);

	@SqlQuery("SELECT * FROM pizza")
	@RegisterBeanMapper(Pizza.class)
	List<Pizza> getAll();

	@SqlQuery("SELECT * FROM pizza WHERE id = :id")
	@RegisterBeanMapper(Pizza.class)
	Pizza findById(long id);

	@SqlQuery("SELECT * FROM pizza WHERE name = :name")
	@RegisterBeanMapper(Pizza.class)
	Pizza findByName(String name);

	@SqlUpdate("insert into PizzaIngredientsAssociation values(:idp , :idi)")
	void ajoutIngredient(long idi, long idp);

	@SqlUpdate("DELETE FROM pizza WHERE id = :id")
	void remove(long id);

	@SqlQuery("Select idi from PizzaIngredientsAssociation where idp = :id")
	List<Long> getAllIngredientsbyIdPizza(long id);

}
