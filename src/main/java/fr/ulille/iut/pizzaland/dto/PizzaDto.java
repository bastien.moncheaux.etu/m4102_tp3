package fr.ulille.iut.pizzaland.dto;

import java.util.List;


public class PizzaDto {
	private long id;
	private String name;
	private List<Long> Integers;

	public PizzaDto() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Long> getIntegers() {
		return Integers;
	}

	public void setIntegers(List<Long> Integers) {
		this.Integers = Integers;
	}

}
