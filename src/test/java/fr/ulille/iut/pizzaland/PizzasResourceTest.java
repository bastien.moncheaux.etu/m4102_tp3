package fr.ulille.iut.pizzaland;

import fr.ulille.iut.pizzaland.ApiV1;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.logging.Logger;

/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface javax.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester
 */
public class PizzasResourceTest extends JerseyTest {
    private static final Logger LOGGER = Logger.getLogger(PizzasResourceTest.class.getName());
    private PizzaDao dao;
    @Override
    protected Application configure() {
     BDDFactory.setJdbiForTests();

     return new ApiV1();
  }

  @Before
  public void setEnvUp() {
      dao = BDDFactory.buildDao(PizzaDao.class);
      dao.createTable();
  }

  @After
  public void tearEnvDown() throws Exception {
     dao.dropTable();
  }

  @Test
  public void testGetExistingPizza() {

      Pizza Pizza = new Pizza();
      Pizza.setName("anglaise");

      long id = dao.insert(Pizza.getName());
      Pizza.setId(id);

      Response response = target("/pizzas/" + id).request().get();

      assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

      Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
      assertEquals(Pizza, result);
  }


    @Test
    public void testGetEmptyList() {
	// La méthode target() permet de préparer une requête sur une URI.
	// La classe Response permet de traiter la réponse HTTP reçue.
        Response response = target("/pizzas").request().get();

	// On vérifie le code de la réponse (200 = OK)
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		  
	// On vérifie la valeur retournée (liste vide)
	// L'entité (readEntity() correspond au corps de la réponse HTTP.
	// La classe javax.ws.rs.core.GenericType<T> permet de définir le type
	// de la réponse lue quand on a un type complexe (typiquement une liste).
	List<PizzaDto> Pizzas;
        Pizzas = response.readEntity(new GenericType<List<PizzaDto>>(){});

        assertEquals(0, Pizzas.size());

    }
    @Test
    public void testGetNotExistingPizza() {
      Response response = target("/pizzas/125").request().get();
      assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
    }
    
    @Test
    public void testCreatePizza() {
        PizzaCreateDto PizzaCreateDto = new PizzaCreateDto();
        PizzaCreateDto.setName("italienne");

        Response response = target("/pizzas")
                .request()
                .post(Entity.json(PizzaCreateDto));

        // On vérifie le code de status à 201
        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

        // On vérifie que le champ d'entête Location correspond à
        // l'URI de la nouvelle entité
        assertEquals(target("/pizzas/" +
    		returnedEntity.getId()).getUri(), response.getLocation());
    	
    	// On vérifie que le nom correspond
        assertEquals(returnedEntity.getName(), PizzaCreateDto.getName());
    }
    	
    @Test
    public void testCreateSamePizza() {
        PizzaCreateDto PizzaCreateDto = new PizzaCreateDto();
        PizzaCreateDto.setName("italienne");
        dao.insert(PizzaCreateDto.getName());

        Response response = target("/pizzas")
                .request()
                .post(Entity.json(PizzaCreateDto));

        assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
    }

    @Test
    public void testCreatePizzaWithoutName() {
        PizzaCreateDto PizzaCreateDto = new PizzaCreateDto();

        Response response = target("/pizzas")
                .request()
                .post(Entity.json(PizzaCreateDto));

        assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());

    }
    @Test
    public void testDeleteExistingPizza() {
        Pizza Pizza = new Pizza();
        Pizza.setName("italienne");
        long id = dao.insert(Pizza.getName());
        Pizza.setId(id);

        Response response = target("/pizzas/" + id).request().delete();

        assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

        Pizza result = dao.findById(id);
    	assertEquals(result, null);
    }

    @Test
    public void testDeleteNotExistingPizza() {
        Response response = target("/pizzas/125").request().delete();
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
    	response.getStatus());
    }
    @Test
    public void testGetPizzaName() {
        Pizza Pizza = new Pizza();
        Pizza.setName("italienne");
        long id = dao.insert(Pizza.getName());

        Response response = target("pizzas/" + id + "/name").request().get();

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        assertEquals("italienne", response.readEntity(String.class));
    }

    @Test
    public void testGetNotExistingPizzaName() {
        Response response = target("pizzas/125/name").request().get();

        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }
    @Test
    public void testCreateWithForm() {
        Form form = new Form();
        form.param("name", "texane");

        Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        Response response = target("pizzas").request().post(formEntity);
        
        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
        String location = response.getHeaderString("Location");
        long id = Integer.parseInt(location.substring(location.lastIndexOf('/') + 1));
        Pizza result = dao.findById(id);
    	
        assertNotNull(result);
    }

    @Test
    public void testAjoutIngredientsByPizza() {
        Form form = new Form();
        form.param("name", "texane");

        Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        Response response = target("pizzas").request().post(formEntity);
        
        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
        String location = response.getHeaderString("Location");
        long id = Integer.parseInt(location.substring(location.lastIndexOf('/') + 1));
        Pizza result = dao.findById(id);
    	
        assertNotNull(result);
    }


    
    

}
